# Repository for automatic segmentation of middle ear OCT data

It contains script that can convert .nrrd files to .nii.gz files (if needed), run inference using nnUNet. The intermediate .nii.gz file(s) will be saved under `./data/<TIME_STRING>/input` folder by default, the predicted label(s) (also in .nii.gz format) can be found under `./data/<TIME_STRING>/output` folder.

## Usage:

- Prediction of a single .nrrd or .nii.gz file (if the to be predicted image is .nii.gz format, it should have "_0000" at the end of the filename to specify the modality index, here we only have single modality, so always set it to 0):
```
python3 inference.py --input /PATH/TO/NRRD/XXX.nrrd 
```
or
```
python3 inference.py --input /PATH/TO/NIIGZ/XXX_0000.nii.gz 
```


- Prediction of multiple .nrrd or .nii.gz files, a folder where the data lie should be given, and the flag `multi` should be set:
```
python3 inference.py --input /FOLDER/OF/DATA --multi
```

