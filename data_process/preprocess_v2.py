import json
import os
import argparse
import glob
import utils
import numpy as np
import nrrd
import shutil

def criteria_folder(x):
    return ("Kopie" not in x) and ("raw" not in x) and (" " not in x) and (x.startswith("2"))


def criteria_segment_name(x):
    return x.startswith("Segment") and x.endswith("_Name")



def get_segment_path(data):
    folder = data["folder"]
    res = {}
    for k in data["sample"].keys():
        print(k)
        # print(data["sample"][k])
        segment_folder_left_0 = os.path.join(folder, k, data["sample"][k]["left"][0])
        segment_folder_right_0 = os.path.join(folder, k, data["sample"][k]["right"][0])
        segment_folder_left_1 = list(filter(lambda x: criteria_folder(x), os.listdir(segment_folder_left_0)))[0]
        segment_folder_right_1 = list(filter(lambda x: criteria_folder(x), os.listdir(segment_folder_right_0)))[0]
        segment_folder_left_1 = os.path.join(segment_folder_left_0, segment_folder_left_1)
        segment_folder_right_1 = os.path.join(segment_folder_right_0, segment_folder_right_1)

        # print(segment_folder_left_1, segment_folder_right_1)

        segment_paths_left = glob.glob(os.path.join(segment_folder_left_1,  "*JM.seg.nrrd")) + glob.glob(os.path.join(segment_folder_left_1,  "*CS.seg.nrrd"))
        # print(segment_paths)
        segment_paths_right = glob.glob(os.path.join(segment_folder_right_1,  "*JM.seg.nrrd")) + glob.glob(os.path.join(segment_folder_right_1,  "*CS.seg.nrrd"))
        res[k] = {
            "left": segment_paths_left,
            "right": segment_paths_right
        }
    return res


segment_name_list = [
    "tympanic membrane",
    "malleus",
    "incus",
    "stapes",
    "promontory",
]


def get_label_index(filelist):
    res = {}
    for f in filelist.keys():
        sides = ["left", "right"]
        res[f] = {}
        for idx_s, side in enumerate(sides):
            paths = filelist[f][side]
            # res[f][side] = [{k:v for k,v in zip(segment_name_list, ["", ] * len(segment_name_list))}, ] * len(paths)
            # res[f][side] = [{"filepath": "", "index": ""}, ] * len(paths)
            res[f][side] = []
            for idx_p, p in enumerate(paths):
                # res[f][side][idx_p] = {"filepath": p, "index":{}}
                res[f][side].append( {"filepath": p, "index":{k:v for k,v in zip(segment_name_list, [-1, ] * len(segment_name_list))} } )
                nrrd_data, nrrd_meta = utils.get_nrrd_data(filename=p)
                # nrrd_meta = json.dumps(nrrd_meta, cls=utils.NumpyEncoder, indent=4)
                # print(json.dumps(nrrd_meta, cls=utils.NumpyEncoder, indent=4))
                # print(nrrd_meta.keys())
                # segment_keys = list(filter(lambda x:criteria_segment_name(x), nrrd_meta.keys()))
                # print(segment_keys)
                # for seg_name in segment_keys:
                #     print(seg_name, nrrd_meta[seg_name])
                segment_keys = list(filter(lambda x:str(nrrd_meta[x]).lower() in segment_name_list and "Name" in x, nrrd_meta.keys()))
                # print(segment_keys)
                for seg_name in segment_keys:
                    print(seg_name, nrrd_meta[seg_name])
                label_value_keys = [k.split("_")[0] + "_LabelValue" for k in segment_keys]
                print("before", res[f][sides[idx_s]][idx_p]["index"])
                for idx_l, l in enumerate(label_value_keys):
                    # print(l, nrrd_meta[l])
                    # print(sides[idx_s])
                    seg_idx = int(nrrd_meta[l])
                    if nrrd_meta[l] == "":
                        seg_idx = -1
                    print(nrrd_meta[segment_keys[idx_l]].lower(), seg_idx)
                    res[f][sides[idx_s]][idx_p]["index"][nrrd_meta[segment_keys[idx_l]].lower()] = seg_idx
                    # print(res[f])
                print("after", res[f][sides[idx_s]][idx_p]["index"])
    return res





def reformat_segment( indices, output_folder=""):
    for id in indices.keys():
        sides = ["left", "right"]
        for idx_side, s in enumerate(sides):
            for idx_f in range(len(indices[id][s])):
                filepath = indices[id][s][idx_f]["filepath"]
                data, _ = utils.get_nrrd_data(filepath)
                print(data.shape)
                data_new = np.zeros(data.shape, dtype=int)
                cur_index_list = indices[id][s][idx_f]["index"]
                for idx_s, seg in enumerate(segment_name_list):
                    if not cur_index_list[seg] == -1:
                        data_new [data == cur_index_list[seg]] = idx_s + 1
                
                print("np.unique:", np.unique(data_new))
                    # if seg in cur_index_list.keys():
                        # print(seg, cur_index_list[seg], idx_s + 1)
                        # if not cur_index_list[seg] == idx_s + 1:
                        #     data[data == cur_index_list[seg]] = idx_s + 1
                if output_folder:
                    filename = "D2EAR_" + str(id) + str(idx_side) + str(idx_f) + ".nrrd"
                    output_path = os.path.join(output_folder, filename )
                    nrrd.write(filename=output_path, data=data_new, header=None, index_order='C')
                    print("saved reformated nrrd to:", output_path)




def get_image_paths(data):
    folder = data["folder"]
    res = {}
    for k in data["sample"].keys():
        print(k)
        # print(data["sample"][k])
        segment_folder_left_0 = os.path.join(folder, k, data["sample"][k]["left"][0])
        segment_folder_right_0 = os.path.join(folder, k, data["sample"][k]["right"][0])
        segment_folder_left_1 = list(filter(lambda x: criteria_folder(x), os.listdir(segment_folder_left_0)))[0]
        segment_folder_right_1 = list(filter(lambda x: criteria_folder(x), os.listdir(segment_folder_right_0)))[0]
        segment_folder_left_1 = os.path.join(segment_folder_left_0, segment_folder_left_1)
        segment_folder_right_1 = os.path.join(segment_folder_right_0, segment_folder_right_1)

        # print(segment_folder_left_1, segment_folder_right_1)

        # segment_paths_left = glob.glob(os.path.join(segment_folder_left_1,  "*JM.seg.nrrd")) + glob.glob(os.path.join(segment_folder_left_1,  "*CS.seg.nrrd"))
        # # print(segment_paths)
        # segment_paths_right = glob.glob(os.path.join(segment_folder_right_1,  "*JM.seg.nrrd")) + glob.glob(os.path.join(segment_folder_right_1,  "*CS.seg.nrrd"))
        image_path_left = glob.glob(os.path.join(segment_folder_left_1,  "*TNode.nrrd"))
        image_path_right = glob.glob(os.path.join(segment_folder_right_1,  "*TNode.nrrd"))
        res[k] = {
            "left": image_path_left,
            "right": image_path_right
        }
    return res


def reformat_image(image_paths, output_folder=""):
    for k in image_paths.keys():
        for idx_side, s in enumerate(["left", "right"]):
            image_path = image_paths[k][s][0]
            print(image_path)
            if output_folder:
                output_path_0 = os.path.join(output_folder, "D2EAR_" + k + str(idx_side) + str(0) + "_0000.nrrd")
                output_path_1 = os.path.join(output_folder, "D2EAR_" + k + str(idx_side) + str(1) + "_0000.nrrd")
            # shutil.copy2(src=image_path)

                shutil.copy2(src=image_path, dst=output_path_0)
                shutil.copy2(src=image_path, dst=output_path_1)
                print("copied to:", output_path_0)
                print("copied to:", output_path_1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('path', help='path to .json file')

    args = parser.parse_args()

    if os.path.exists(args.path):
        with open(args.path, 'r') as file:
            data = json.load(file)
    
    print(json.dumps(data, indent=4))

    # print(json.dumps(data, indent=4))
    # paths = get_segment_path(data=data)
    # print(json.dumps(paths, indent=4))
    # indices = get_label_index(paths)
    # print(json.dumps(indices, indent=4))

    # with open(args.path.replace(".json", "_w_idx.json"), "w") as f:
    #     json.dump(indices, f)

    # with open(args.path.replace(".json", "_w_idx.json"), "r") as f:
    #     indices = json.load(f)

    # output_folder = os.path.join(data["folder"], "reformatted")
    # reformat_segment(indices=indices, output_folder=output_folder)

    image_paths = get_image_paths(data=data)
    print(json.dumps(image_paths, indent=4))

    output_folder = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/Probanden/reformatted/images"
    reformat_image(image_paths=image_paths, output_folder=output_folder)

