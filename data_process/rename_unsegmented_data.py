import os
import glob
import shutil


def rename_grouped_0():
    folder_path = "/home/liupeng/ceph_home/data/D2EAR_data/Probanden_unsegmented/grouped_0/"
    file_path_list = glob.glob(folder_path + "ID*")
    # print(file_path_list)
    output_folder = "/home/liupeng/ceph_home/data/D2EAR_data/Probanden_unsegmented/grouped_0/renamed_nnunet"

    for p in file_path_list:
        filename = os.path.basename(p)
        print(filename)
        id = filename[3:6]
        l_or_r = 0 if filename[7] == "l" else 1
        print(id, l_or_r)

        filename_new = "D2EAR_{}{}0_0000.nrrd".format(id, l_or_r)
        # print(filename_new)
        output_path = os.path.join(output_folder, filename_new)
        # print(output_path)
        shutil.copy(src=p, dst=output_path)
        print("copied to:", output_path)


def rename_grouped_1(folder_path, output_folder):
    # folder_path = "/home/liupeng/ceph_home/data/D2EAR_data/Probanden_unsegmented/grouped_0/"
    file_path_list = glob.glob(folder_path + "ID*")
    # print(file_path_list)
    # output_folder = "/home/liupeng/ceph_home/data/D2EAR_data/Probanden_unsegmented/grouped_0/renamed_nnunet"

    for p in file_path_list:
        filename = os.path.basename(p)
        print(filename)
        id = filename[3:6]
        l_or_r = 0 if filename[7] == "l" else 1
        print(id, l_or_r)

        filename_new = "D2EAR_{}{}0_0000.nrrd".format(id, l_or_r)
        # print(filename_new)
        output_path = os.path.join(output_folder, filename_new)
        # print(output_path)
        shutil.copy(src=p, dst=output_path)
        print("copied to:", output_path)



if __name__ == "__main__":
    # REMARK: no need to rename, test old name
    folder_path =  "/home/liupeng/ceph_home/data/D2EAR_data/Probanden_unsegmented/grouped_1/"
    output_folder = "/home/liupeng/ceph_home/data/D2EAR_data/Probanden_unsegmented/grouped_1/renamed_nnunet"
    rename_grouped_1(folder_path=folder_path, output_folder=output_folder)

