
import os
import sys
sys.path.append("../")
import utils



def check_old_data():
    folder = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/"
    image_path = os.path.join(folder, "imagesTr", "D2EAR_18700_0000.nrrd")
    label_path = os.path.join(folder, "labelsTr", "D2EAR_18700.nrrd")
    image_meta = utils.get_nrrd_header(image_path)
    label_meta = utils.get_nrrd_header(label_path)

    print(image_meta)

    print("========================")
    print(label_meta)



def check_new_data():
    folder = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/Probanden/reformatted/2023_07_27/JM"
    image_path = os.path.join(folder, "imagesTr", "D2EAR_18700_0000.nrrd")
    label_path = os.path.join(folder, "labelsTr", "D2EAR_18700.nrrd")
    # label_path = os.path.join(folder, "labelsTr_cleaned", "D2EAR_18700.nrrd")


    image_meta = utils.get_nrrd_header(image_path)
    label_meta = utils.get_nrrd_header(label_path)

    print(image_meta)

    print("========================")
    print(label_meta)



if __name__ == "__main__":
    check_old_data()
