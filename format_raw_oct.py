import os
import glob
import shutil
import json
import numpy as np
import nibabel as nib
import traceback
import nrrd


def copy_to_single_folder():
    data_folder = "/home/liupeng/ceph_home/data/D2EAR_data/raw_oct_data/Probanden/"
    target_folder = "/home/liupeng/ceph_home/data/D2EAR_data/raw_oct_data/nrrd_ordered"

    file_path = glob.glob(os.path.join(data_folder, "**/*.nrrd"), recursive=True)
    output_list = []
    for f in file_path:
        print("=================")
        print(f)
        if "Segment" in f:
            print("segmentation file, go next")
            continue
        # print(f.split("/")[8])
        idx = 0
        output_path = os.path.join(target_folder, "{}_{}.nrrd".format(f.split("/")[8], idx))

        while os.path.exists(output_path):
        # while output_path in output_list:
            idx += 1
            output_path = os.path.join(target_folder, "{}_{}.nrrd".format(f.split("/")[8], idx))
        print("output_path", output_path)
        shutil.copy2(src=f, dst=output_path)
        output_list.append([f, output_path])
    print(len(output_list))
    
    with open(os.path.join(target_folder, "filename_map.json"), "w") as json_file:
        json.dump(output_list, json_file)
        json_file.close()



def convert_nrrd_to_niigz_single(src_filename, dst_filename):

    try:
        print("input:", src_filename)
        _nrrd = nrrd.read(src_filename)
        data = _nrrd[0]
        header = _nrrd[1]

        img = nib.Nifti1Image(data, np.eye(4))

        print("output: {}".format(dst_filename))
        nib.save(img, os.path.join(dst_filename))
    except Exception as e:
        traceback.print_exc()




def format_nnunet():
    data_folder = "/home/liupeng/ceph_home/data/D2EAR_data/raw_oct_data/nrrd_ordered"
    output_folder = "/home/liupeng/ceph_home/data/D2EAR_data/raw_oct_data/niigz_files"
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    filepath_list = glob.glob(os.path.join(data_folder, "*.nrrd"))
    # filename_map = []
    for idx_f, f in enumerate(filepath_list):
        # print(idx_f, f, f.split("/")[-1].split(".")[0])
        src = f
        dst = os.path.join(output_folder, "{}.nii.gz".format(f.split("/")[-1].split(".")[0]))

        print(src, dst)
        convert_nrrd_to_niigz_single(src_filename=src, dst_filename=dst, )



if __name__ == "__main__":
    format_nnunet()


