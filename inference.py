import os, sys, yaml
import utils
# from nnUNet import nnunet
from nnUNet.nnunet.inference import predict as nnunet_predict
import nrrd
import nibabel as nib
import numpy as np
import glob



def decorator_timer(some_function):
    from time import time

    def wrapper(*args, **kwargs):
        t1 = time()
        some_function(*args, **kwargs)
        end = time()-t1
        print("Functions took {:3f}s".format(end))
        return end
    return wrapper


@decorator_timer
def convert_nrrd_to_niigz(nrrd_path, output_folder):
    print("converting {} to .nii.gz format for training".format(nrrd_path))
    _nrrd = nrrd.read(nrrd_path)
    data = _nrrd[0]
    # header = _nrrd[1]
    #save nifti
    img = nib.Nifti1Image(data, np.eye(4))
    output_path = os.path.join(output_folder, os.path.splitext(os.path.basename(nrrd_path))[0] + "_0000.nii.gz")
    print("saving to {}".format(output_path))
    nib.save(img, output_path)
    return output_path


@decorator_timer
def predict(param, input_folder, output_folder):
    print(param)
    nnunet_predict.predict_from_folder(
        param["nnunet"]["model_folder_name"], 
        input_folder,  
        output_folder, 
        param["nnunet"]["folds"], 
        param["nnunet"]["save_npz"], 
        param["nnunet"]["num_threads_preprocessing"], 
        param["nnunet"]["num_threads_nifti_save"],
        param["nnunet"]["lowres_segmentations"],
        param["nnunet"]["part_id"],
        param["nnunet"]["num_parts"],
        not param["nnunet"]["disable_tta"],         
        overwrite_existing=param["nnunet"]["overwrite_existing"],
        mode=param["nnunet"]["mode"], 
        overwrite_all_in_gpu=param["nnunet"]["overwrite_all_in_gpu"], 
        mixed_precision=not param["nnunet"]["mixed_precision"],
        step_size=param["nnunet"]["step_size"],  
        checkpoint_name=param["nnunet"]["checkpoint_name"],
        )


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Scripts to run automatic segmentation for OCT data")
    parser.add_argument("--param_path", default="parameters.yml", required=False, type=str, help="path to the yaml file where parameters are saved")
    parser.add_argument("--input", type=str, help="Path to the .nrrd file(s) (OCT volume)")
    parser.add_argument("--output_folder", default="./data", required=False, type=str, help="Folder where the converted .nii.gz file and predicted segmentation file lies")
    parser.add_argument("--multi", action='store_true', )
    args = parser.parse_args()
    param_path = args.param_path
    input = args.input
    output_folder = args.output_folder
    multi = args.multi


    time_str = utils.get_time_str()
    if os.path.exists(input) and os.path.exists(output_folder):
        os.makedirs(os.path.join(output_folder, time_str, "input"))
        os.makedirs(os.path.join(output_folder, time_str, "output"))
    else:
        print("Path not exists")
        sys.exit()

    print(multi)
    convert = False
    if not multi:
        input_file_list = [input,]
        print( os.path.basename(input)[-5:])
        if os.path.basename(input)[-5:] == ".nrrd":
            convert = True
    else:
        input_file_list = glob.glob(os.path.join(input, "*.nii.gz"))
        if not input_file_list:
            input_file_list = glob.glob(os.path.join(input, "*.nrrd"))
            convert = True

    # convert .nrrd to .nii.gz
    if convert:
        print("=======================================")
        print("|               Converting            |")
        print("=======================================")
        print(input_file_list)  
        for nrrd_path in input_file_list:
            convert_nrrd_to_niigz(nrrd_path=nrrd_path, output_folder=os.path.join(output_folder, time_str, "input"))

    print("=======================================")
    print("|               Predicting            |")
    print("=======================================") 
    param = utils.load_parameter(param_path)
    nnunet_input_folder = os.path.dirname(input)
    if convert:
        nnunet_input_folder = os.path.join(output_folder, time_str, "input")
    print("nnunet_input_folder",  nnunet_input_folder)
    predict(
        param, 
        input_folder=nnunet_input_folder,
        output_folder=os.path.join(output_folder, time_str, "output")
    )


    # print("=======================================")
    # print("|  Converting prediction to 3D model  |")
    # print("=======================================") 




    # print("=======================================")
    # print("|            Visualizing              |")
    # print("=======================================") 

