
import os
import utils
import glob
import nrrd

def rename(folder):
    file_list = os.listdir(folder)
    for f in file_list:
        new_filename = "D2EAR_" + f.split(".")[0] + "_0000.nrrd"
        print(new_filename)
        os.rename(os.path.join(folder, f), os.path.join(folder, new_filename))

def rename_back(folder):
    file_list = os.listdir(folder)
    for f in file_list:
        new_filename = f[6:].replace("_0000", "")
        print(new_filename)
        os.rename(os.path.join(folder, f), os.path.join(folder, new_filename))


def rename_label(folder):
    file_list = os.listdir(folder)
    for f in file_list:
        new_filename = f.replace("_0000", "")
        print(new_filename)
        os.rename(os.path.join(folder, f), os.path.join(folder, new_filename))



def update_header ():
    # get example header
    filepath = "/home/liupeng/ceph_home/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/imagesTr/D2EAR_18700_0000.nrrd"
    # _, meta = utils.get_nrrd_data(filepath)
    meta = utils.get_nrrd_header(filepath)
    print(meta)

    filepath = "/home/liupeng/ceph_home/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/labelsTr/D2EAR_18700.nrrd"
    meta_label = utils.get_nrrd_header(filepath)
    print(meta_label)

    meta_label['space'] = meta["space"]
    meta_label['space directions'] = meta['space directions']
    meta_label['space origin'] = meta['space origin']

    print(meta_label)


    folder = "/home/liupeng/ceph_home/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/labelsTr/"
    output_folder = "/home/liupeng/ceph_home/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/labelsTr_test"
    file_list = glob.glob(os.path.join(folder, "*.nrrd"))
    print(file_list)
    for f in file_list:
        data, _ = utils.get_nrrd_data(filename=f)
        output_path = os.path.join(output_folder, os.path.basename(f))
        nrrd.write(filename=output_path, data=data, header=meta_label, index_order='C')
        print("output to", output_path)


if __name__ == "__main__":
    # folder = "/home/liupeng/ceph_home/data/D2EAR_data/Probanden/reformatted"
    # folder = "/home/liupeng/ceph_home/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/labelsTr"
    # rename_label(folder=folder)
    update_header()
