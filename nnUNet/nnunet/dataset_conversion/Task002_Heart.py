
import os


def rename_folder(sub_folder_name):
    data_base = os.path.join(os.environ["nnUNet_raw_data_base"], "nnUNet_raw_data", "Task002_Heart")
    print(data_base)

    # filenames

    sub_folder = os.path.join(data_base, sub_folder_name)
    for f in os.listdir(sub_folder):
        print(f)
        if f.endswith("nii.gz"):
            print(os.path.join(sub_folder, f[:-7] + "_0000.nii.gz"))
            os.rename(os.path.join(sub_folder, f), os.path.join(sub_folder, f[:-7] + "_0000.nii.gz"))


if __name__ == "__main__":
    rename_folder(sub_folder_name="imagesTs")

