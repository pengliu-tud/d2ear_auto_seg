import nrrd
import traceback, os
import nibabel as nib
import numpy as np


def read_nrrd_info(filename, verbose=False):
    try:
        data, header = nrrd.read(filename=filename, index_order='C')
        if verbose:
            print("filepath:", filename)
            print("data shape:", data.shape)

        # print(type(data))
        # print(data.shape)
        return data
    except Exception as e:
        traceback.print_exc()

def read_niigz(filename, verbose=False):
    try:
        img = nib.load(filename)
        if verbose:
            width, height, queue = img.dataobj.shape
            print("vlume size:", width, height, queue)
        return img.dataobj
    except Exception as e:
        traceback.print_exc()


def get_label_numbers(folder):
    try:
        seg_file_list = os.listdir(folder)
        for f_s in seg_file_list:
            # if f_s.endswith(".nii.gz"):
            if f_s.endswith(".seg.nrrd"):
                print(f_s)
                # volume = read_niigz(os.path.join(folder, f_s))
                volume = read_nrrd_info(os.path.join(folder, f_s))
                print(np.unique(volume))
    except Exception as e:
        traceback.print_exc()





if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Utils for nrrd files")
    parser.add_argument("filepath", type=str, help="Path to the nrrd file")

    args = parser.parse_args()
    filepath = args.filepath

    get_label_numbers(filepath)
