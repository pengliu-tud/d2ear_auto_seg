import numpy as np
import nrrd
import os
import utils
import vtk
import glob
import json



label_name = [
    "1_Tympanic_Membrane",
    "2_Malleus",
    "3_Incus",
    "4_Stapes",
    "5_Promontory",
]


@utils.decorator_timer
def convert_vtk(label_map_path, output_folder=None):
    # Step 1: Load the label map and create VTK image data
    label_map, label_map_header = nrrd.read(label_map_path)
    label_map = np.transpose(label_map, axes=(2, 1, 0))  # Transpose to match VTK's ordering

    # Get the dimensions of the label map
    dims = label_map.shape
    spacing = label_map_header['space directions']

    label_list = np.unique(label_map)

    for idx_s in label_list:
        if idx_s == 0:
            continue
        print("Converting label", idx_s)
        label = (label_map == idx_s).astype(np.uint8)

        # Create a VTK image data
        image_data = vtk.vtkImageData()
        image_data.SetDimensions(dims[0], dims[1], dims[2])
        # image_data.SetSpacing([spacing[0,0], spacing[1,1], spacing[2,2]])
        image_data.SetSpacing([0.019, 0.0078125, 0.019])
        image_data.AllocateScalars(vtk.VTK_UNSIGNED_CHAR, 1)  # We use an unsigned char array for binary data

        # Copy the label map data into the VTK image data

        flat_label_map = label.flatten()

        # Convert the flattened label map to a VTK unsigned char array
        vtk_data_array = vtk.vtkUnsignedCharArray()
        vtk_data_array.SetNumberOfTuples(len(flat_label_map))
        vtk_data_array.SetVoidArray(flat_label_map, len(flat_label_map), 1)

        # Set the VTK data array as the scalars for the VTK image data
        image_data.GetPointData().SetScalars(vtk_data_array)

        # Step 3: Extract the surface using the Marching Cubes algorithm
        marching_cubes = vtk.vtkMarchingCubes()
        marching_cubes.SetInputData(image_data)
        marching_cubes.SetValue(0, 1.0)  # Extract the isosurface at the value 1.0 (structure)

        if output_folder:
            # Step 4: Save the mesh to an STL file
            output_path = os.path.join(output_folder, "{}.stl".format(label_name[idx_s - 1]))
            stl_writer = vtk.vtkSTLWriter()
            stl_writer.SetFileName(output_path)
            stl_writer.SetInputConnection(marching_cubes.GetOutputPort())
            stl_writer.Write()
            print("saved to:", output_path)



def convert_pyvista():
    import pyvista as pv
    nrrd_path = "/mnt/cluster/datasets/D2EAR/inference_fullres_split/1-10/ID_157_l_2023-04-13_12-17-08_2023-04-13_13-59.nrrd"
    nrrd_path = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/Probanden/187/ID_187_l_2022-11-21_11-58-15/2022-11-21_18-30/ID_187_l_2022-11-21_11-58-15_2022-11-21_18-30_JM.seg.nrrd"

    data, header = utils.get_nrrd_data(nrrd_path)

    # print(header)
    print(json.dumps(header, indent=4, sort_keys=False, cls=utils.NumpyEncoder))  

    origin = header["space origin"]
    space_directions = header['space directions']

    grid = pv.UniformGrid()
    grid.dimensions = data.shape
    grid.origin = origin
    # grid.spacing = (space_directions[2,2], space_directions[1,1], space_directions[0,0])
    grid.spacing = (0.019, 0.0078125, 0.019)
    grid.point_data["values"] = data.ravel(order="F")

    # Step 3: Convert the UniformGrid to a surface mesh
    surf = grid.extract_geometry()

    # Step 4: Save the surface mesh to an STL file
    output_folder = "data_for_test"
    output_file_path = os.path.join(output_folder, "output_mesh.stl")
    surf.save(output_file_path)
    print("saved to:", output_file_path)








if __name__ == "__main__":
    nrrd_path_1 = "/mnt/cluster/datasets/D2EAR/inference_fullres_split/1-10/ID_157_l_2023-04-13_12-17-08_2023-04-13_13-59.nrrd"
    nrrd_path_2 = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/Probanden/187/ID_187_l_2022-11-21_11-58-15/2022-11-21_18-30/ID_187_l_2022-11-21_11-58-15_2022-11-21_18-30_JM.seg.nrrd"
    # # Replace "label_map.nrrd" and "output_mesh.stl" with your file paths


    # data, header = utils.get_nrrd_data(nrrd_path_1)
    # # print(np.unique(data))
    # print(header["space directions"])

    # data, header = utils.get_nrrd_data(nrrd_path_2)
    # print(header["space directions"])


    # output_folder = "data_for_test"
    # # output_file_path = os.path.join(output_folder, "output_mesh.stl")
    # convert_vtk(nrrd_path_1, output_folder=output_folder)

    import argparse
    parser = argparse.ArgumentParser(description="Convert nrrd to stl")
    parser.add_argument("--nrrd_folder", type=str, required=True, help="folder where .nrrd files are saved")
    parser.add_argument("--output_folder", type=str, default="", help="Folder where to save the converted stl files (subfolder with the name of the nrrd files will be created)")

    args = parser.parse_args()
    nrrd_folder = args.nrrd_folder
    output_folder = args.output_folder

    nrrd_filepath_list = glob.glob(os.path.join(nrrd_folder, "*.nrrd"))
    print(len(nrrd_filepath_list))

    for filepath in nrrd_filepath_list:
        print("==============================================")
        print("Current file:", filepath)
        output_folder_sample = None
        if output_folder:
            output_folder_sample = os.path.join(output_folder, os.path.basename(filepath).replace(".nrrd", ""))
            print("output folder:", output_folder_sample)
            if not os.path.exists(output_folder_sample):
                os.makedirs(output_folder_sample)
        convert_vtk(
            label_map_path=filepath,
            output_folder=output_folder_sample
        )
    



