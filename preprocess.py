import openpyxl
from openpyxl.cell import MergedCell
import json,os
import shutil
import utils
import numpy as np
import nrrd


def parser_cell(sheet: openpyxl.Workbook, row, col):
    cell = sheet.cell(row=row, column=col)
    value = cell.value
    if isinstance(cell, MergedCell):
        for merged_range in sheet.merged_cells.ranges:
            if cell.coordinate in merged_range:
                # return the left top cell
                cell = sheet.cell(row=merged_range.min_row, column=merged_range.min_col)
                break
        value = cell.value
    return value



def load_protocal(file_path, show=False):
    # Load the workbook
    workbook = openpyxl.load_workbook(file_path)

    # Select the sheet (if you know the name)
    sheet = workbook["Tabelle1"]  # Replace "Sheet1" with the actual sheet name

    # Or, select the first sheet by default
    sheet = workbook.active

    # Read the data from the sheet
    data = []
    cur_row = {}
    title = []
    for idx_r, row in enumerate(sheet.iter_rows(), start=1):
        for idx_c, cell in enumerate(row, start=1):
            v = parser_cell(sheet, row=idx_r, col=idx_c)
            if idx_r == 1:
                title.append(v)
            else:
                cur_row[title[idx_c-1]] = v
            # print(v, end=" ")
        if cur_row:
            data.append(cur_row)
        cur_row = {}
        # print()

    # Print the data
    # for row in data:
    #     print(row)
    if show:
        print(json.dumps(data, indent=4, sort_keys=False))

    # Close the workbook (don't forget to do this after reading)
    workbook.close()

    return data


def convert_protocal_to_json_segmented_samples(protocal, data_folder, output_path=None):
    """Extract segmented samples from protocal and save the path information into json file
        Conditions:
            - checked by Joseph
            - Path according to the protocal

    Args:
        protocal (list): list of .xlsx rows
        data_folder (str): path to the dataset folder
        output_path (str, optional): where to save the json file. Defaults to None.

    Returns:
        _type_: _description_
    """    
    sample_finished = []
    for sample in protocal:
        # print(sample)
        if sample["Überprüfung Joseph"] == "ok":
            sample_finished.append(sample)
    print(len(sample_finished))

    # create path list
    path_list = {}
    for s in sample_finished:
        if not s["ID"] in path_list.keys():
            path_list[s["ID"]] = {
                "left": {     
                    "image": None,
                    "segmentation":{
                        "JM": None,   
                        "CS": None,
                        "SST": None,
                    }
                },
                "right": {    
                    "image": None,
                    "segmentation":{    
                        "JM": None,   
                        "CS": None,
                        "SST": None,
                    }
                },
            }
        folder = os.path.join(data_folder, str(s["ID"]), s["Datensatz"]) 
        # print(folder, os.listdir(folder))
        for sub_dir in os.listdir(folder):
            if sub_dir.startswith("20") and "Kopie" not in sub_dir:
                # print(sub_dir)
                # folder = os.path.join(folder, sub_dir,)
                print(s["ID"], s["Seite"])
                image_path = os.path.join(folder, sub_dir, s["Prozessierung"] + ".nrrd")
                seg_CS = os.path.join(folder, sub_dir, s["Prozessierung"] + "_CS.seg.nrrd")
                seg_JM = os.path.join(folder, sub_dir, s["Prozessierung"] + "_JM.seg.nrrd")
                if os.path.exists(image_path):
                    path_list[s["ID"]][s["Seite"]]["image"] = image_path
                if os.path.exists(seg_CS):
                    path_list[s["ID"]][s["Seite"]]["segmentation"]["CS"] = seg_CS
                else:
                    path_list[s["ID"]][s["Seite"]]["segmentation"]["SST"] = os.path.join(folder, sub_dir, s["Prozessierung"] + "_SST.seg.nrrd")
                path_list[s["ID"]][s["Seite"]]["segmentation"]["JM"] = seg_JM
                # print(path_list[s["ID"]][s["Seite"]]["JM"])

    # for p in path_list:
    #     print(p, os.path.exists(p))
    print(json.dumps(path_list, indent=4, sort_keys=False))
    if output_path:
        with open(output_path, "w") as file:
            file.write(json.dumps(path_list))
        print("saved to :", output_path)
    return path_list



def convert_protocal_to_json_unsegmented_samples(protocal, data_folder, output_path=None):
    """Explore unsegmented or unchecked samples, save the path information to json. Used to created validation sets
        Conditions:
            - Shown up in the protocal (some samples only have IDs listed, no folder information)
            - Not checked by Joseph
            - Iteratively explore the subfolders of the samples to find possible images 
                + inlcude all possible samples
                + only include the first sample
    
    Args:
        protocal (_type_): _description_
        data_folder (_type_): _description_
        output_path (_type_, optional): _description_. Defaults to None.
    """    
    samples_unsegmented = []
    for sample in protocal:
        if sample["ID"] and not sample["Überprüfung Joseph"] == "ok" :
            samples_unsegmented.append(sample)
    # print(samples_unsegmented)
    print(json.dumps(samples_unsegmented, indent=4, sort_keys=False))
    print(len(samples_unsegmented))

    path_list = {}
    for s in samples_unsegmented:
        if not s["ID"] in path_list.keys():
            path_list[s["ID"]] = {
                "image_path_list": [],
            }
        # iteratively explore subfolders:
        cur_folder = os.path.join(data_folder, str(s["ID"]), )
        print("current folder:", cur_folder)
        
        # list all record folders
        subfolders_record = [ os.path.join(cur_folder, f)  for f in os.listdir(cur_folder)]

        # filder valid record folders by finding folders do not only contain "raw"
        subfolders_record = list(filter( 
            lambda x: len(os.listdir(x)) > 1,
            subfolders_record
        ))

        # find image filename:
        image_path_list = []
        for sub_f_r in subfolders_record:
            subfolder_process = os.listdir(sub_f_r)
            if "raw" in subfolder_process:
                subfolder_process.remove("raw")
            subfolder_process = [os.path.join(sub_f_r, p)  for p in subfolder_process]
            print(subfolder_process)

            for sub_f_p in subfolder_process:
                if os.path.isdir(sub_f_p):
                    filename_list = list(filter(
                        lambda x: str(x).startswith("ID") and str(x).endswith(".nrrd") and not "TNode" in str(x) and not "seg" in str(x),
                        os.listdir(sub_f_p)
                    ))
                    filename_list = [ os.path.join(sub_f_p, f) for f in filename_list]
                    image_path_list.extend(filename_list)
        path_list[s["ID"]]["image_path_list"] = image_path_list
            
    print(json.dumps(path_list, indent=4, sort_keys=False))
    if output_path:
        with open(output_path, "w") as file:
            file.write(json.dumps(path_list))
        print("saved to :", output_path)

    return path_list


def copy_files_segmented(protocal, output_folder, sides=["left", "right"], annotators=["JM"]):
    """Save the original segmentations as the format required by nnUnetV2
        filename example: D2EAR_18701.nrrd    
                        {187}{0}{1}:
                        187 :   ID; 
                        0   :   which side, 0 is left, 1 is right; 
                        1   :   which annotator, 0:JM, 1:CS; 2:SST

    Args:
        protocal (dict): dict format of valid segmentations
        output_folder (str): folder where to save the segmentation in nnUnetV2 format
        sides (list, optional): which sides to save, both or left only or right only. Defaults to ["left", "right"].
    """
    output_folder_image = os.path.join(output_folder, "imagesTr")
    output_folder_segmentation = os.path.join(output_folder, "labelsTr")
    if not os.path.exists(output_folder_image):
        os.makedirs(output_folder_image)
    if not os.path.exists(output_folder_segmentation):
        os.makedirs(output_folder_segmentation)
    for id in protocal.keys():
        print(id)
        for idx_s, side in enumerate(protocal[id].keys()):
            if side in sides:
                print(side)
                for idx_an, annotator in enumerate(protocal[id][side]["segmentation"].keys()):
                    if annotator in annotators and protocal[id][side]["segmentation"][annotator] and os.path.exists(protocal[id][side]["segmentation"][annotator]):
                        # save segmentations:
                        print(protocal[id][side]["segmentation"][annotator])
                        source_path = protocal[id][side]["segmentation"][annotator]
                        output_path = os.path.join(output_folder_segmentation, "D2EAR_{}{}{}.nrrd".format(id, idx_s, idx_an))
                        # print(output_path)
                        shutil.copy2(src=source_path, dst=output_path)
                        print("saved to:", output_path)

                        # save original images:
                        if os.path.exists(protocal[id][side]["image"]):
                            source_path = protocal[id][side]["image"]
                            output_path = os.path.join(output_folder_image, "D2EAR_{}{}{}_0000.nrrd".format(id, idx_s, idx_an))
                            shutil.copy2(src=source_path, dst=output_path)
                            print("saved to:", output_path)


def copy_files_unsegemnted(protocal, target_folder):
    for id in protocal.keys():
        print(id)
        source_filepath_list = protocal[id]["image_path_list"]
        for source_filepath in source_filepath_list:
            target_filepath = os.path.join(target_folder, os.path.basename(source_filepath).split(".")[0] + "_0000" + os.path.basename(source_filepath).split(".")[1])
            shutil.copy2(src=source_filepath, dst=target_filepath)
            print("copied to:", target_filepath)


def rename_unsegmented_files(folder):
    """Rename the unsegmented files to "*_0000.nrrd" like 

    Args:
        folder (str): folder where the samples are
    """
    import glob
    filename_list = glob.glob(os.path.join(folder, "*.nrrd"))
    for filename in filename_list:
        if not filename.endswith("_0000.nrrd"):
            os.rename(src=filename, dst=filename.replace(".nrrd", "_0000.nrrd"))
            print("renamed {} to {}".format(filename, filename.replace(".nrrd", "_0000.nrrd")))





def filter_labels(folder, output_folder=None):

    segment_name_list = [
        "tympanic membrane",
        "malleus",
        "incus",
        "stapes",
        "promontory",
    ]

    from glob import glob
    nrrd_path_list = glob(os.path.join(folder, "*.nrrd"))
    for path in nrrd_path_list:
        print(path)
        nrrd_data, nrrd_meta = utils.get_nrrd_data(filename=path)
        # nrrd_meta = json.dumps(nrrd_meta, cls=utils.NumpyEncoder, indent=4)
        # print(json.dumps(nrrd_meta, cls=utils.NumpyEncoder, indent=4))

        # for segment_name in segment_name_list:
        # segment_keys = list(filter(lambda x : str(nrrd_meta[x]).lower() in segment_name_list and "Name" in x, nrrd_meta.keys()))
        segment_name_keys = {
            "tympanic membrane" :   "",
            "malleus"           :   "",
            "incus"             :   "",
            "stapes"            :   "",
            "promontory"        :   "",
        }
        for k in nrrd_meta.keys():
            if str(nrrd_meta[k]).lower() in segment_name_keys.keys():
                segment_name_keys[str(nrrd_meta[k]).lower()] = k
            if str(nrrd_meta[k]).lower() == "promotory":
                segment_name_keys["promontory"] = k

        segment_name_labelvalues = {
            "tympanic membrane" :   -1,
            "malleus"           :   -1,
            "incus"             :   -1,
            "stapes"            :   -1,
            "promontory"        :   -1,
        }

        for name in segment_name_labelvalues.keys():
            if segment_name_keys[name]:
                key_labelvalue = segment_name_keys[name].split("_")[0] + "_LabelValue"
                segment_name_labelvalues[name] = int(nrrd_meta[key_labelvalue])
        
        print(segment_name_labelvalues)
        for v in segment_name_labelvalues.values():
            if v == -1:
                print(json.dumps(nrrd_meta, cls=utils.NumpyEncoder, indent=4))

        # segment_keys_prefix = [ seg.split("_")[0] for seg in segment_name_keys.values()]
        # segment_keys_labelvalues = [seg + "_LabelValue" for seg in segment_keys_prefix]
        # segment_values_labelvalues = [int(nrrd_meta[seg]) for seg in segment_keys_labelvalues]
        # print(segment_keys)
        # print(segment_values_labelvalues)

        data_new = np.zeros(nrrd_data.shape, dtype=int)
        for idx, k in enumerate(segment_name_labelvalues.keys()):
            if not segment_name_labelvalues[k] == -1:
                data_new[ nrrd_data == segment_name_labelvalues[k] ] = idx + 1
        
        print("np.unique:", np.unique(data_new), )
        # for idx_s, seg in enumerate(segment_name_list):
        #     if idx_s < len(segment_values_labelvalues):
        #         data_new [nrrd_data == segment_values_labelvalues[idx_s]] = idx_s + 1
        
        # print("np.unique:", np.unique(data_new))
        if output_folder:
            output_path = os.path.join(output_folder, os.path.basename(path))
            nrrd.write(filename=output_path, data=data_new, header=None, index_order='C')
            print("saved to:", output_path)



def update_header(folder, output_folder=None):
    from glob import glob
    filepath = "/home/liupeng/ceph_home/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset003_D2EARJMONLY/imagesTr/D2EAR_18700_0000.nrrd"
    # _, meta = utils.get_nrrd_data(filepath)
    meta_image = utils.get_nrrd_header(filepath)
    print(meta_image)

    filepath = "/home/liupeng/ceph_home/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset003_D2EARJMONLY/labelsTr/D2EAR_18700.nrrd"
    meta_label = utils.get_nrrd_header(filepath)
    print("before update:")
    print(meta_label)
    meta_label['space'] = meta_image["space"]
    meta_label['space directions'] = meta_image['space directions']
    meta_label['space origin'] = meta_image['space origin']
    print("after update:")
    print(meta_label)

    file_list = glob(os.path.join(folder, "*.nrrd"))
    print(file_list)
    for f in file_list:
        data, _ = utils.get_nrrd_data(filename=f)
        if output_folder:
            output_path = os.path.join(output_folder, os.path.basename(f))
            nrrd.write(filename=output_path, data=data, header=meta_label, index_order='C')
            print("output to", output_path)








if __name__ == "__main__":
    filepath = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/annotation_protocal/Segmentierungen.xlsx"

    data_folder = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/Probanden/"
    reformat_folder = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/Probanden/reformatted/2023_07_27/"
    reformat_folder = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/Probanden/reformatted/unsegmented/2023_08_01/"

    # read protocal from xlsx file
    protocal = load_protocal(filepath, show=False)

    # convert to json for easy access
    # protocal = convert_protocal_to_json_segmented_samples(
    #     protocal=protocal, 
    #     data_folder=data_folder,
    #     output_path=os.path.join(data_folder, "protocal_2023_07_27.json"),
    # )

    # reformat the files
    # copy_files_segmented(
    #     protocal=protocal,
    #     sides=["left", "right"],
    #     annotators=["JM"],
    #     output_folder=os.path.join(reformat_folder, "JM"),
    # )

    # reorganize segmentation labels
    # filter_labels(
    #     folder=os.path.join(reformat_folder, "JM", "labelsTr"),
    #     output_folder=os.path.join( reformat_folder, "JM", "labelsTr_cleaned"),
    # )

    # update spacings:
    # update_header(
    #     folder=os.path.join(reformat_folder, "JM", "labelsTr_cleaned"),
    #     output_folder=os.path.join(reformat_folder, "JM", "labelsTr_cleaned"),
    # )

    # protocal = convert_protocal_to_json_unsegmented_samples(
    #     protocal=protocal,
    #     data_folder=data_folder,
    #     output_path=os.path.join(reformat_folder, "protocal_unsegmented_2023_08_01.json"),
    # )

    # copy_files_unsegemnted(
    #     protocal=protocal, 
    #     target_folder=os.path.join(reformat_folder, "test_full"),
    #     )

    rename_unsegmented_files(
        folder="/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/Probanden/reformatted/unsegmented/2023_08_01/test_full_data/",
    )




# nnUNetv2_predict -d Dataset003_D2EARJMONLY -i /home/liupeng/ceph_home/data/D2EAR_data/Probanden/reformatted/unsegmented/2023_08_01/test_full_data/ -o /home/liupeng/ceph_home/data/D2EAR_data/Probanden/reformatted/unsegmented/2023_08_01/test_full_inference/ -f  0 1 2 3 4 -tr nnUNetTrainer -c 3d_fullres -p nnUNetPlans