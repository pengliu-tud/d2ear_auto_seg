import slicer, os
import argparse


print("hello")

# parser = argparse.ArgumentParser(description="Scripts to convert nnUnet prediction to 3D segmentation for visualization")
# parser.add_argument("--inference_path", required=True, type=str, help="path to the yaml file where parameters are saved")
# parser.add_argument("--output_folder", type=str, help="Path to the .nrrd file(s) (OCT volume)")

    

# inference_seg_path = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnet/nnUNet_raw_data_base/nnUNet_raw_data/Task500_D2EAR/imagesTs/ear_024.nii.gz"
# inference_gt_path = "/home/liupeng/ceph_home/data/D2EAR_data/data_collection_with_segmentation_formatted/samples/reorder_segmentations/ear_024.nrrd"
# inference_gt_path = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/data_collection_with_segmentation_formatted/samples/reorder_segmentations/ear_024.nrrd"
# segmentation_path = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnet/nnUNet_raw_data_base/nnUNet_raw_data/Task500_D2EAR/imagesTs/ear_024.seg.nrrd"
# segmentation_path = inference_gt_path
# segmentation_path = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/data_collection_reordered_by_surgeons/original_samples/Svea_li/Segmentation_for_ML.seg.nrrd"
# segmentation_path = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnet/nnUNet_trained_models/nnUNet/3d_fullres/Task520_D2EAR0708/nnUNetTrainerV2__nnUNetPlansv2.1/fold_3/validation_raw_postprocessed/ear172.nii.gz"
# segmentation_path = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnet/nnUNet_trained_models/nnUNet/3d_fullres/Task520_D2EAR0708/nnUNetTrainerV2__nnUNetPlansv2.1/fold_1/validation_raw_postprocessed/ear163.nrrd"
# segmentation_path = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/Probanden/187/ID_187_l_2022-11-21_11-58-15/2022-11-21_18-30/ID_187_l_2022-11-21_11-58-15_2022-11-21_18-30_JM.seg.nrrd"
# folder ="/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/imagesTs_unsegmented_predicted/"
folder = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/imagesTs_predicted/"
folder = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/imagesTs_unsegmented_1_predicted/"
folder = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/imagesTs_predicted_fold_4/"
# for filename in ["D2EAR_19700.nrrd",  "D2EAR_19810.nrrd",  "D2EAR_20000.nrrd",  "D2EAR_20110.nrrd",  "D2EAR_20400.nrrd",  "D2EAR_20700.nrrd",  "D2EAR_20810.nrrd",
# "D2EAR_19710.nrrd",  "D2EAR_19900.nrrd",  "D2EAR_20010.nrrd",  "D2EAR_20300.nrrd",  "D2EAR_20410.nrrd",  "D2EAR_20710.nrrd",
# "D2EAR_19800.nrrd",  "D2EAR_19910.nrrd",  "D2EAR_20100.nrrd",  "D2EAR_20310.nrrd",  "D2EAR_20610.nrrd",  "D2EAR_20800.nrrd",]:
# for filename in ["D2EAR_19510.nrrd"]:
# filename_list = ["D2EAR_15710.nrrd", "D2EAR_20200.nrrd", "D2EAR_20210.nrrd", "D2EAR_20900.nrrd"]
filename_list = ["D2EAR_19510.nrrd"]
for filename in filename_list:
    segmentation_path =  folder + filename
    slicer.util.loadSegmentation(segmentation_path)

    segmentation_name = segmentation_path.split("/")[-1].split(".")[0]
    print("segmentation_name", segmentation_name)

    segmentation_node = slicer.util.getNode(segmentation_name)


    # output_folder = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnet/nnUNet_raw_data_base/nnUNet_raw_data/Task500_D2EAR/imagesTs/output_seg"
    # output_folder = os.path.join(os.path.dirname(segmentation_path), "output_slicer_code")
    # output_folder = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/imagesTs_unsegmented_predicted/stl"
    output_folder = os.path.join(folder, "stl")
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    spacing = 1.0
    # spacing = 0.016
    # spacing = (0.019000000000000003, 0.0078125, 0.019000000000000003)
    slicer.modules.segmentations.logic().ExportSegmentsClosedSurfaceRepresentationToFiles(output_folder, segmentation_node, None,"STL", True, spacing, False )
    slicer.modules.segmentations.logic().ExportSegmentsClosedSurfaceRepresentationToFiles(output_folder, segmentation_node, None,"STL", True, spacing, True )

exit()


# Command:
#  xvfb-run -s  "-screen 0 1400x900x24"  ./Slicer --no-main-window --python-script /mnt/ceph/tco/TCO-Staff/Homes/liupeng/Code/D2EARAutoSeg/slicer_convert.py


