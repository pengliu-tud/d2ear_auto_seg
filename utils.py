import yaml 
from datetime import datetime
import nrrd
import json
import traceback
import numpy as np
import SimpleITK as sitk


def decorator_timer(some_function):
    from time import time

    def wrapper(*args, **kwargs):
        t1 = time()
        some_function(*args, **kwargs)
        end = time()-t1
        print("Functions took {:3f}s".format(end))
        return end
    return wrapper



def load_parameter(yml_path):
    with open(yml_path, "r") as f:
        param = yaml.safe_load(f)
        f.close()
    return param



def get_time_str():
    return datetime.now().strftime("%Y_%m_%d_%H_%M_%S")


def get_nrrd_data(filename):
    try:
        # data, header = nrrd.read(filename=filename, index_order='C')
        data, header = nrrd.read(filename=filename, index_order='C')
        print("filepath:", filename)
        print("data shape:", data.shape)
        # print(header)
        # # print(type(data))
        # # print(data.shape)
        return data, header
    except Exception as e:
        traceback.print_exc()


def get_nrrd_header(filename):
    try:
        header = nrrd.read_header(filename)
        print("filepath:", filename)

        return header
    except Exception as e:
        traceback.print_exc()


def get_nrrd_itk(filename):


    reader = sitk.ImageFileReader()
    reader.SetImageIO("NrrdImageIO")
    reader.SetFileName(filename)
    image = reader.Execute()
    print(image.GetSpacing())

    return image
    # import itk
    # import matplotlib.pyplot as plt
    # from monai.transforms import LoadImage

    # out = LoadImage(reader="itkreader", pixel_type=itk.Vector[itk.F,3])("./example.seg.nrrd")
    # print(out[0].shape)
    # plt.imshow(out[0][..., 0])
    # plt.show()

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)



if __name__ == "__main__":
    # print(get_time_str())
    filename = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/D2EAR_data/Probanden/187/ID_187_l_2022-11-21_11-58-15/2022-11-21_18-30/ID_187_l_2022-11-21_11-58-15_2022-11-21_18-30_JM.seg.nrrd"
    image =get_nrrd_itk(filename)
    print(image.GetSize())
    image.SetSpacing([0.019,] * 3)
    print(image.GetSize())
    image = sitk.GetArrayFromImage(image)
    # 
    print(image.shape)
