import matplotlib.pyplot as plt
import numpy as np
import traceback
import utils
import imageio
import os
import json

def show_nrrd_single(data):
    # show scans one by one
    print(data.shape)
    num = 1
    im = None
    for i in range(0, data.shape[0], 10):
    # for i in range(0, queue, 1):
        # print(queue, i)

        # img_arr = img.dataobj[:, :, i]
        # plt.subplot(width, height, num)
        # plt.imshow(img_arr, cmap='gray', vmin=0, vmax=10)
        # num += 1

        image = data[i, ...]
        # print(width, i)
        print(i, np.sum(image))
        # image = img.dataobj[i, ...]
        if not im:
            # im = plt.imshow(image, cmap='gray')
            im = plt.imshow(image, cmap='gray', vmin=0, vmax=np.max(data))
        else:
            im.set_data(image)
        plt.pause(.1)
        plt.draw()



# def show_nrrd_multiple(data_list):
#     for data in data_list:

def show_single_image_from_single_nrrd(image_volume, index=0, axis=0, show=False, save=False):
    try:
        fig = plt.figure()
        ax = fig.gca()
        fig.tight_layout()
        if axis == 0:
            image_frame = image_volume[index, ...]
        elif axis == 1:
            image_frame = image_volume[:, index, ...]
        elif axis == 2:
            image_frame = image_volume[..., index]
        ax.axis('off')
        ax.imshow(image_frame, cmap='gray')
        fig.canvas.draw()       # draw the canvas, cache the renderer
        # plt.subplots_adjust(0,0,1,1,0,0)
        image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
        if save:
            fig.savefig("outputs/images_oct/{}.jpg".format(index), bbox_inches='tight')
        w, h = fig.canvas.get_width_height()
        image  = image.reshape([w, h, 3])
        if show:
            plt.show()
        return image
    except Exception as e:
        traceback.print_exc()



def show_single_combined_images_from_multiple_nrrd(image_volume_list, index=0,  axis=0, show=True, save=True):
    try:
        fig, ax = plt.subplots(1,len(image_volume_list))
        image_frame_list = []
        
        for idx, image_volume in enumerate(image_volume_list):
            if axis == 0:
                image_frame = image_volume[index, ...]
            elif axis == 1:
                image_frame = image_volume[:, index, ...]
            elif axis == 2:
                image_frame = image_volume[..., index]       
            image_frame_list.append(image_frame)
            ax[idx].imshow(image_frame, cmap='gray')
        plt.show()

        image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
        print(image.shape)
        w, h = fig.canvas.get_width_height()
        image  = image.reshape([w, h, 3])
        print(image.shape)
        return image 
    except Exception as e:
        traceback.print_exc()


def show_multiple_combined_images_from_multiple_nrrd(image_volume_list,  axis=0, show=True, output_folder="", x_labels=[], y_labels=[]):
    fig, ax = plt.subplots(1, len(image_volume_list),  figsize=(3 * len(image_volume_list), 6))
    image_frame_list = []
    if not x_labels:
        x_labels = ["",] * len(image_volume_list)
    if not y_labels:
        y_labels = ["",] * len(image_volume_list)
    for idx_f in range(0, data.shape[axis], 10):
        for idx, image_volume in enumerate(image_volume_list):
            if axis == 0:
                image_frame = image_volume[idx_f, ...]
            elif axis == 1:
                image_frame = image_volume[:, idx_f, ...]
            elif axis == 2:
                image_frame = image_volume[..., idx_f]       
            # image_frame_list.append(image_frame)
            ax[idx].imshow(image_frame)
            if idx > 0:
                ax[idx].set_yticks([])
            # if x_labels[idx]:
            ax[idx].set(xlabel=x_labels[idx], ylabel=y_labels[idx])

        plt.pause(.1)
        plt.draw()
    
        # 
        # print(sum(image))
        if output_folder:
            fig.savefig(os.path.join(output_folder, "{}.png".format(idx_f)), bbox_inches='tight')
            print("saved to", os.path.join(output_folder, "{}.png".format(idx_f)) )
        image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
        
        # print(image.shape) 
    #     w, h = fig.canvas.get_width_height()
    #     image  = image.reshape([w, h, 3])
    #     print(image.shape)
    #     image_frame_list.append(image)
    
    # imageio.mimsave(os.path.join(output_folder, 'animation.gif'), image_frame_list, fps=10)
    # print("saved gif to", os.path.join(output_folder, 'animation.gif'))

    # return image 


def gif_from_folder(folder):
    filenames = list(filter(lambda x:x.endswith(".png"), os.listdir(folder)))
    filenames = sorted(filenames, key=lambda x: int(x.split(".")[0]))
    print(filenames)

    with imageio.get_writer(os.path.join(folder, "animation.gif"), mode='I', fps=4) as writer:
        for f in filenames:
            p = os.path.join(folder, f)
            print(p)
            image = imageio.imread(p )
            writer.append_data(image)




if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    # parser.add_argument('path', help='path to .json file')
    parser.add_argument('--path', nargs='+', help='Path to image volume', required=True)
    parser.add_argument('--output_folder', type=str, default="pics", help='Path to output_folder',)

    args = parser.parse_args()

    path_list = args.path
    output_folder = args.output_folder
    print(path_list)

    # data, _ = utils.get_nrrd_data(args.path)
    # show_nrrd_single(data=data)

    # path_list = [
    #     "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/imagesTr/D2EAR_18700_0000.nrrd",
    #     "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/labelsTr/D2EAR_18700.nrrd",
    #     "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/labelsTr/D2EAR_18701.nrrd"
    # ]

    # # idx = 196
    # idx = 195
    # path_list = [
    #     "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/imagesTs/D2EAR_{}10_0000.nrrd".format(idx),
    #     # "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/labelsTr/D2EAR_{}10.nrrd".format(idx),
    #     "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/labelsTr/D2EAR_{}11.nrrd".format(idx),
    #     "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnetV2/nnUNet_raw/Dataset002_D2EARDatasetV2/imagesTs_predicted/D2EAR_{}11.nrrd".format(idx),
    # ]

    output_folder = os.path.join(output_folder, utils.get_time_str())
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    with open(os.path.join(output_folder, "image_path.json"), "w") as f:
        json.dump(obj=path_list, fp=f)
        f.close()

    image_volume_list = []
    for p in path_list:
        data, _ = utils.get_nrrd_data(p)
        image_volume_list.append(data)
    # # show_combined_images_from_multiple_nrrd(image_volume_list=image_volume_list, index=200, axis=0)
    show_multiple_combined_images_from_multiple_nrrd(
        image_volume_list=image_volume_list, 
        output_folder=output_folder,
        # x_labels=["oct image", "segmented by CS", "segmented by JM", "prediction from NN"],
        # x_labels=["oct image", "segmented by JM", "prediction from NN"],
        # y_labels=[]
        )

    gif_from_folder(folder=output_folder)


